FROM alpine:latest

ARG CDKTF_VERSION=latest
ARG TF_VERSION=1.5.2
ARG PYTHON_VERSION='3.7.0'
ARG PYENV_HOME=/root/.pyenv

RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
			npm \
			curl \
			git \
			make \
			g++ \
			bash
RUN curl -o terraform.zip https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip
RUN unzip terraform.zip
RUN mv terraform /usr/bin/terraform

# Add pytho for alpine
RUN apk add --no-cache python3

RUN npm install --global cdktf-cli@${CDKTF_VERSION}

RUN adduser -D -u 1000 -g 1000 user
USER user

CMD [ "cdktf" ]