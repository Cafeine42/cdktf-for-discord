import { Construct } from "constructs";
import { App, TerraformStack, HttpBackend, HttpBackendConfig } from "cdktf";
import { DiscordProvider, DiscordProviderConfig } from "./.gen/providers/discord/provider";
import { TextChannel, TextChannelConfig } from "./.gen/providers/discord/text-channel";
import { CategoryChannel, CategoryChannelConfig } from "./.gen/providers/discord/category-channel";
import { VoiceChannel, VoiceChannelConfig } from "./.gen/providers/discord/voice-channel";

class MyStack extends TerraformStack {
  constructor(scope: Construct, id: string) {
    super(scope, id);

    const token = (process.env.DISCORD_TOKEN as string);
    const serverId = (process.env.DISCORD_SERVER_ID as string);

    const discordConfig: DiscordProviderConfig = {
      token: token,
    };

    new DiscordProvider(this, "discord", discordConfig);

    const categoryChannelConfig: CategoryChannelConfig = {
      name: "category-channel",
      serverId: serverId,
      position: 0,
    };

    const categoryChannel = new CategoryChannel(this, "category-1", categoryChannelConfig);

    const textChannelConfig: TextChannelConfig = {
      name: "text-channel",
      topic: "This is a text channel",
      serverId: serverId,
      category: categoryChannel.id,
    };

    new TextChannel(this, "text-channel", textChannelConfig);

    const voiceChannelConfig: VoiceChannelConfig = {
      name: "text-channel",
      serverId: serverId,
      category: categoryChannel.id,
    };

    new VoiceChannel(this, "voice-channel", voiceChannelConfig);
  }
}

const app = new App();
const stack = new MyStack(app, "cdktf-discord");

const PROJECT_ID = process.env.CI_PROJECT_ID;
const TF_STATE_ADDRESS = "https://gitlab.com/api/v4/projects/"+PROJECT_ID+"/terraform/state/cdktf-discord"
const BACKEND_USERNAME = process.env.BACKEND_USERNAME || 'gitlab-ci-token';
const BACKEND_PASSWORD = process.env.CI_JOB_TOKEN || process.env.BACKEND_PASSWORD;

const httpBackendConfig: HttpBackendConfig = {
    address: TF_STATE_ADDRESS,
    username: BACKEND_USERNAME,
    password: BACKEND_PASSWORD,
    lockAddress: TF_STATE_ADDRESS + "/lock",
    unlockAddress: TF_STATE_ADDRESS + "/lock",
    lockMethod: "POST",
    unlockMethod: "DELETE",
    retryWaitMin: 5,
};

new HttpBackend(stack, httpBackendConfig);

app.synth();
