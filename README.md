# Discord setup with CDK for Terraform

Example project for CDK For Terraform usage with [Discord provider](https://registry.terraform.io/providers/aequasi/discord/latest/docs).
This project use typescript and is based on [CDK for Terraform](https://learn.hashicorp.com/tutorials/terraform/cdktf).

The project come with :
- a ready to work Docker container with CDK for Terraform installed
- a working Gitlab CI.

## Requirements

- [Docker](https://www.docker.com/)
- [Docker login with Gitlab](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html)

## Usage

Copy the file compose.override.yml.example to compose.override.yml and fill the environment variables.
```bash
cp compose.override.yml.example compose.override.yml
```

Launch docker container
```bash
docker compose up -d
```

Enter the container
```bash
docker exec -it cdktf-for-discord-1 ash
```

Install npm dependencies and generate .gen from provider
```bash
npm install
cdktf get
```

For more help
```bash
cdktf
```